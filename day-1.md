##### **Ecrire du JavaScript**

- Méthodes pas propre
Ajouter notre javascript au niveau de notre HTML 
  
````html
<html>
<head>
    
</head>
<body>
<script>alert('Ecrire le JavaScript ICI')</script>
</body>
</html>

````
- Méthodes propre

Il faut séparer nos fichiers (un pour le JS, un pour le HTML)
Pour inclure un fichier JS dans une page HTML : 
- On ajoute notre fichier JS juste avant la fermeture de notre balise body
- On utilise une balise script avec un attribut rel égal à "script" et l'attribut src qui représente le chemin vers notre fichier

````html
<html>
    <head></head>
    <body>
        <script rel="script" src="mon-fichier.js"></script>
    </body>
</html>
````
On écrira donc notre code JavaScript dans le fichier mon-fichier.js


##### **Variables**

- var : 
    Ancienne méthode permettant de créer des variables.
    On peut redéclarer une variable qui existe déjà (l'ancienne variable sera alors écrasée).
    La portée de var sera globale (on pourra y accéder en dehors du block courant)
  
````javascript
    // Initialisation et affectation de ma variable
    var maVariable = 'Ma valeur'
    
    if(maVariable == 'Ma valeur'){
        var maVariable = 'Ma nouvelle valeur';
    }
    
    // La console affichera : 'Ma nouvelle valeur' puisque la portée de var ne se limite pas au block courant
    console.log(maVariable);
````

- let
````javascript
    // Initilisation d'une variable avec let : 
    let maVar;
    
    // Initialisation ET affectation d'une valeur à ma variable
    let maVar2 = 'Ma valeur';
    
    // ATTENTION : Avec let on ne peut pas initialiser 2 fois la même variable. L'instruction ci-dessous déclanche une erreur :
    let maVar = 'Une nouvelle variable';
    
    // La portée d'une variable initialisé avec let se limite au bloc courant 

    let maVariable = 'Ma valeur';
    
    if(maVariable == 'Ma valeur'){
        let maVariable = 'Ma nouvelle valeur';
    }
    
    // La console affichera : Ma valeur
    console.log(maVariable);
````
- const

const défini une constante. 
- On ne pourra pas changer la valeur une fois qu'elle lui est assigné. 
- Utilise moins de ressource 
- Une constante est une variable en lecture seule. 
- La portée de const est limitée au bloc courant. 
- On ne peut pas initialiser une constante sans lui attribuer une valeur
````javascript
    const maConstante = 'Ma valeur';
    
    // Il sera impossible de changer la valeur. L'instruction ci-dessous déclanchera une erreur
    maConstante = 'Nouvelle valeur';
````

##### **Types**

JavaScript est un langage faiblement typé.
En JavaScript, le type de notre variable est automatiquement assigné en fonction de la valeur que l'on passe. 
On a pas besoin de spécifier le type de notre variable lors de l'initialisation. 
Une même variable peut changer de type au cours du cycle de vie du programme. 

Attention, en fonction du type de notre variable, JavaScript peut avoir un comportement différents. 

Exemple : 

````javascript
let variable1 = '1';
let variable2 = '2';

// Le code ci-dessous affichera 12
console.log(variable1 + variable2);

let variable1 = 1;
let variable2 = 2;

// Le code ci-dessous affichera 3
console.log(variable1 + variable2);
````

Les différents types en JavaScript : 
- number (représente les entiers ET les nombres à virgule)
  
````javascript
// Exemple de type number
 let monNombre = 1;
// Exemple de type number
 let monFloat = 1.19;
````

- string (représente une chaine de caractères). Pour les définir, on peut utiliser des quotes ou des guillemets

````javascript
let maChaine = 'Une chaine de caractères';
let maChaine2 = "Une autre chaine de caractères";

// On peut utiliser le \ pour échapper des caractères exemple : 
let maChaine3 = 'Aujourd\'hui nous sommes le 24 mars';

````
  
- booléen (valeur vrai ou faux)
````javascript
    let boolVrai = true;
    let boolFalse = false;
`````

- undefined (Une variable qui n'a pas encore de type)
````javascript
    // Ici ma var n'a pas encore de type ... Elle aura un type indéfini
    let maVar;
`````

- null (représente une variable qui a un type mais pas de valeur)

Pour afficher le type d'une variable, on peut utiliser typeof

````javascript
    let maVar = 'Hello world';
    // Affichera : string
    console.log(typeof maVar);

    let maVar2 = 1;
    // Affichera : number
    console.log(typeof maVar2);

    let maVar3 = true;
    // Affichera : booleen
    console.log(typeof maVar3);
````

- type objet : Type plus complexe qui pourra contenir plusieurs éléments de types différents. 


##### **Debug**

On peut utiliser la console 
- onglet network pour vérifier le chargement des source JS
- onglet console (affichage des erreurs, affichage des logs)
- onglet Element (debug du HTML)

Instruction debugger qui sert à faire du développement pas à pas 
````javascript
debugger;
let maVar = 1; 
// ...
````

## **ALGO ! **



##### **Conditions**

````javascript
if(condition){
    
} else if (condition2){
    
} else {
    
}
````

**- Les comparateurs :** 
- == : Vérifie une égalité de valeur. Les variables peuvent avoir un type différents
  
````javascript
var1 = '10';
var2 = 10;

if(var1 == var2){
    console.log('JE PASSE BIEN ICI !');
}
````
- === : Vérifie une égalité de valeur ET de type
````javascript
var1 = '10';
var2 = 10;

if(var1 === var2){
console.log('Je ne passe pas ici');
} else {
    console.log('Ja pase ici');
}
````
- var1 < var2 : Vérifie que var1 est inférieur à var2
 ````javascript
var1 = 9;
var2 = 10;

if(var1 < var2){
    console.log('Ja pase ici');
}
```` 
- var 1 > var2  : Vérifie que var1 est supérieur à var2

 ````javascript
var1 = 10;
var2 = 9;

if(var1 > var2){
    console.log('Ja pase ici');
}
```` 

- var1 <= var2 : Vérifie que var1 est inférieur ou égal à var2 

````javascript
var1 = 10;
var2 = 10;

if(var1 <= var2){
    console.log('Ja pase ici');
}
````
- var1 >= var2 : Vérifie que var1 est supérieur ou égal à var2 
````javascript
var1 = 10;
var2 = 10;

if(var1 >= var2){
    console.log('Ja pase ici');
}
````

- var1 != var2 : vérifie que la valeur de var1 est différente de la valeur de var2
  
````javascript
var1 = 10;

var2 = "10";

if(var1 != var2){
    // Je ne passe pas ici 
} else {
    // Je passerais ici puisque la valeur est la même 
}
````

- var1 !== var2 : Vérifie que la valeur ou le type de var1 est différent de var2

````javascript
var1 = 10;

var2 = "10";

if(var1 != var2){
    // Je passse ici puisque le type n'est pas le même
}
````

- On peut combiner plusieurs conditions en utilisant des opérateurs logiques

```javascript
if(condition1 && condition2){
    // Je passe ici si condition1 est vrai ET si condition2 est vrai
}

if(condition1 || condition2){
    // Je passe ici si condition1 est vrai OU si condition2 est vrai
}

```

##### **Fonctions**

Une fonction est une boite noire qui peut prendre des arguments en entré et peut retourner des valeurs en sortie

On utilise des fonctions pour éviter la duplication de code

Facilite la maintenance de l'application 

````javascript
// La fonction ci-dessous prend 2 argument
function additionner(a,b){
    // Elle retourne le resultat d'une addition
    return a+b;
}

// Appel de la fonction : 
// Ici la variable resultat sera égale à 3
let resultat1 = additionner(1,2);
// resultat2 sera égal à 4
let resultat2 = additionner(2,2);
````

##### **Switch**

Permet de vérifier des conditions à la chaine
Pour l'utiliser : 

````javascript
let maVar = 'Pomme';

switch(maVar){
    case 'Tartine':
        // Je passse ici si maVar est égal à tartine
        // Le break permet de sortir de mon switch lorsque j'ai trouvé une correspondance
        break;
    case 'Pomme': 
        // Je vais passer ici : maVar est égal à Pomme
        break;
    default: 
        // Je passe ici si je ne suis passé dans aucun des cas
}
````

##### **Tableaux**

- Tableaux numérotés exemple : liste de course on empile des éléments

````javascript
let array = ["Pomme", "Banane", "Nutella"];

// Affichera Pomme
console.log(array[0]);
````

La premiere valeur du tableau sera à l'index 0

Il existe différentes méthodes en JS pour manipuler nos tableaux

 push (ajoute un element dans un tableau)slice
 
pop

 unshift 
 ... 

- Tableaux clés => valeurs

Notre index sera une chaine de caractères

````javascript
let tab = [];
tab['nom'] = 'Delorme';
tab['prenom'] = 'Aurélien';
// Affichera Delorme
console.log(tab["nom"]);
````

Nos tableaux peuvent être combiner (utilisation de tableaux clé => valeur dans un tableau numéroté)

Exemple : Un tableau contenant plusieurs identités ! 

````javascript
let students = [];

student1 = [];
student1["nom"] = 'Delorme';
student1["prenom"] = 'Aurélien';

student2 = [];
student2["nom"] = 'Delorme';
student2["prenom"] = 'Aurélien';

students.push(student1);
students.push(student2);
````

##### **Boucles**

- Boucle for qui prend 3 paramètres une initialisation, une condition et un increment
En changeant l'incrément, on peut aller de X en X
```javascript
// Affichera dans la console les chiffres de 1 à 100
for(let i = 1; i<= 100; i++){
    console.log(i);
}
```


```javascript
// Affichera les multiples de 8 jusqu'à 100
for(let i = 1; i<= 100; i+=8){
    console.log(i);
}
```

- While prend en paramètre une condition. Tant que la condition n'est pa validée, la boucle continue

```javascript
let i = 0;
// Affichera les nombres pairs de 0 à 100
while(i<100){
    i+=2
    console.log(i);
}
```

- tab.forEach(elem => console.log(elem));
Utilisation d'une fonction fléchée. Elle permet de parcourir un tableau
  
````javascript
let tab = ["Pomme", "Poire", "Nutella"];
// Affichera tous les éléments de notre tableau
tab.forEach(elem => console.log(elem));
````



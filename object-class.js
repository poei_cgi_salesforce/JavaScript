class Ordinateur {
    marque;
    model;
    prix;

    constructor(marque, model, prix) {
        debugger;
        this.marque = marque;
        this.model = model;
        this.prix = prix;
    }

    additionner(nb1, nb2){
        return nb1+nb2;
    }
}

class OrdinateurFixe extends Ordinateur {
    includeMouse;
    includeKeyboard;

    constructor(marque, model, prix, includeMouse, includeKeyboard) {
        super(marque, model, prix);
        this.includeMouse = includeMouse;
        this.includeKeyboard = includeKeyboard;
    }
}

let ordinateurFixe = new OrdinateurFixe("Dell", "XPS", 1200, true, false);


console.log(JSON.stringify(ordinateurFixe));


let result = ordinateurFixe.additionner(2,2);
console.log(result);
console.log(ordinateurFixe);

/*
debugger;
let ordinateur = new Ordinateur("Apple", "Macbook", 2500.23);
let resultCalculOrdinateur = ordinateur.additionner(1,3);
console.log(resultCalculOrdinateur);

let ordinateur2 = new Ordinateur("Dell", "XPS", 1400);
let result = ordinateur2.additionner(2,2);
*/
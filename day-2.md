###### Javascript jour 2

###### Programmation Orienté Objet

On utilise le POO afin d'utiliser un standard. La POO est plus lisible et le code source est mieux organisé.

Cela permet de favoriser le travail en équipe.

Cela permet d'éviter la duplication de code source.

Un objet est composé de méthodes et d'attributs.


###### 2 types de programmation orienté objet : 

- Basé sur le prototype

````javascript
// Création d'un objet basé sur le prototype 
let monObjet = {
    monAttribut1 : "valeurDeMonAttribut1",
    monAttribut2 : "valeurDeMonAttribut2",
    maFonction: function maFonction(name) {
        console.log("Hello" + name)    
    }   
}
````

Exemple ci-dessus : un objet avec 2 attributs et une méthode

- Basée sur la classe

Apparu avec ES6. Avant on était obligé d'utiliser la POO basée sur le prototype.

1) En JavaScript, on utilise le mot clé classe pour créer une classe
2) Une classe est un moule qui contiendra la structure de nos futurs objets (attributs, mèthodes).

````javascript
class MaClasse {
    monAttribut1;
    monAttribut2;
    
    constructor(monAttribut1, monAttribut2){
        this.monAttribut1 = monAttribut1;
        this.monAttribut2 = monAttribut2;
    }
    
    concatener(){
        return this.monAttribut1 + ' ' + this.monAttribut2;
    }
}
````
###### Créer un objet en JavaScript en utilisant la POO basée sur la classe.

Pour créer un nouvel objet en POO basée sur les classes, j'utilise le mot clé new. Ceci appellera le constructeur.
En paramètre, je passe les différentes valeurs de mes attributs

````javascript
let monObjet = new MaClasse('Valeur attribut 1', 'Valeur attribut 2');
````

###### Constructeurs

Méthode appelé quand on cré un nouvel objet. On a pas besoin de l'appeler il est appelé implicitement lorsque l'on 
utilise le mot clé new. 

Il prend en paramètre les valeurs que l'on souhaite affecté à nos attributs. 


###### Accesseurs

Mèthodes permettant d'accèder à nos attributs et de les modifier 

````javascript
class MaClasse {
    monAttribut1;
    monAttribut2;
    
    // Guetteur qui permet de réccupérer la valeur de mon attribut
    getMonAttribut1(){
        return this.monAttribut1;
    }
    
    // Setteur permettant de mettre à jour la propriété d'un attribut
    setMonAttribut1(monAttribut1){
        this.monAttribut1 =  monAttribut1;
    }
}
````

###### Héritage

Permet à une classe parent de partager ses attributs et ses méthodes avec une classe enfant. 

On l'utilise pour éviter la duplication de code source et ainsi faciliter la maintenance

Lorsque l'on cré un objet enfant, il faut utiliser le constructeur du parent pour construire le parent
Ensuite, on peut surcharger la méthode avec les éléments propre à l'enfant 

````javascript
class MaClasseParent {
    monAttributParent;
    
    constructeur(monAttributParent){
        this.monAttributParent = monAttributParent;
    }
}

class MaClassseEnfant extends MaClasseParent {
    monAttributEnfant;

    constructeur(monAttributParent, monAttributEnfant) {
        super.constructeur(monAttributParent);
        this.monAttributEnfant = monAttributEnfant
    }
}
````


###### this.attribut / this.fonction

En POO, this sert à accéder aux attributs et méthodes au sein de mon objet. 

exemple : dans une classe, this.attribut fera référence à l'attribut attribut de l'objet


#### JSON

JavaScript object Notation 

Permet de réprésenter un objet sous forme de chaine de caractères

Il est très utilisé pour échanger des données 

Les objets seront écrit entre paranthèse {}

Dans un objet JSON, seul les attributs sont représentés 

Exemple : JSON représentant une voiture

````JSON
  {
    "marque": "Audi",
    "modele": "RS5",
    "horses": 350
  }
````

Les listes seront écrites dans des crochets []

Exemple : Liste de voitures : 

````JSON
    [
      {
        "marque": "Audi",
        "modele": "RS5",
        "horses": 350
      },
      {
        "marque": "Renault",
        "modele": "Clio",
        "horses": 80
      }
    ]
````

En JavaScript, on peut utiliser la mèthode stringify de l'objet JSON pour convertir un objet en JSON

Exemple : 

````javascript
// Le resultat monJson sera égal au JSON de monObjet
let monJson = JSON.stringify(monObjet);
````

La méthode JSON.parse permet de transformer une chaine de caractère en objet (processus inverse)

````javascript
// Le resultat monObjet sera égal a l'objet représenté par le Json monJson
let monObjet = JSON.parse(monJson, MaClasse);
````

#### Requêtes sur une API 

GET 

Permet d'obtenir une ou plusieurs ressources 
- lors de l'obtention d'une ressource notre json sera contenu entre {}
  
Exemple : On réccupére l'élément post qui à l'id 2 : 

GET : http://localhost:3000/posts/2
````json
{
    "title": "Titre mis à jour",
    "author": "Aurélien Delorme",
    "id": 2
}
````

Exemple : On réccupére tous les posts de notre API :
GET : http://localhost:3000/posts
````json
[
    {
        "id": 1,
        "title": "json-server",
        "author": "typicode"
    },
    {
        "title": "Titre mis à jour",
        "author": "Aurélien Delorme",
        "id": 2
    },
    {
        "title": "Post 3",
        "author": "Aurélien Delorme",
        "id": 3
    }
]
````
- lors de l'obtention de plusieurs ressources, notre JSON sera contenu entre []


POST 

Permet d'envoyer de la données sur notre API 

La donnée sera envoyée sous forme de JSON dans le body de notre requête HTTP. 

exemple : Sauvegarde d'un objet Post : 

POST : http://localhost:3000/posts

Le body de notre requête HTTP sera du JSON (raw -> JSON) dans postman

````json
{
    "title": "Super post",
    "author": "Aurélien Delorme"
}
````

Dans ce JSON, on ne met pas l'id. l'identifiant sera calculé par le serveur 



PUT / UPDATE / PATCH

Permet de modifier les données 

PUT et l'UPDATE, on modifie l'objet complet 
PATCH on peut modifier l'objet partiellement (seulement les champs que l'on souhaite mettre à jour)

exemple :
PUT http://localhost:3000/posts/3

````json
{
    "title": "Post 3 mis à jour",
    "author": "Aurélien Delorme"
}
````

Cette requête mettrà à jour le post qui a l'id 3 (title et author).

DELETE

Sert à supprimer un objet. Cette méthode ne prend pas de body. Simplement l'id à supprimer dans l'URL 

DELETE : http://localhost:3000/posts/3

Supprime le post qui à l'ID 3.


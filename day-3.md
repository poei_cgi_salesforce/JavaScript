###### BOM

Permet de réccupérer des informations propre au navigateur à travers différents composants

- Géolocalisation. 
  Nous avons 2 fonctions en paramètre. La première sera appelée en cas de succes et prend en paramètre notre navigation
  La deuxième est appelée en cas d'échec (géolocalisation désactivée, position introuvable ...).

````javascript
// Fonction à appeler en cas de success
function  success(pos) {
    // Dans cette variable toutes mes infos de connecté
    console.log(pos);
}

function echec(){
    error.log('impossible de vous localiser');
}
navigator.geolocation.getCurrentPosition(success, echec);
````  

- Langue du navigateur

Permet de connaitre la langue configurée dans le navigateur. Permet d'afficher par défaut la langue de préférence de l'utilisateur 

````javascript
    // La langue préféré de l'utilisateur.
    let langue = navigator.language;
    // Toutes les langues utilisées par l'utilisateur. 
    let langues = navigator.languages;
````

- Historique de navigation
Permet de se rendre sur les pages suivantes, précédente. Tout ce qui est relatif a la navigation sur notre page web
  
```javascript
  // Se rendre sur la page précédente 
  window.history.back();
  // Se rendre sur la page suivante : 
  window.history.forward();
  // Nombre de page consultées par l'utilisateur : 
  window.history.length;
  
```
- Screen

Permet d'identifier le type d'écran et la taille de la feunêtre.
On peut l'utiliser pour faire du responsive,
pour autoriser seulement les smartphones, desktop ...

````javascript
  let infosScreen = window.screen;
````

- Connection

Permet d'avoir des informations sur la connection réseau de l'utilisateur. 
Exemple : Afficher une vidéo youtube avec une qualité optimal (pas trop grosse pour qu'elle se charge vite et avec une bonne qualité si la connexion est bonne)

````javascript
// Reccupérer les infos de connection :
let connexionInformation = navigator.connection;
````
  
- Le DOM

Le dom (document object model) représente notre page web. cf en dessous 


###### DOM

Le DOM correspond à notre page web. Il est représenté sous forme d'arbre
Nous le manipulerons afin de modifier certaines parties de notre page web.

- Selectionner des éléments dans le DOM 

````javascript
// Je selectionne un élément en fonction de son id
// retourne UN objet qui correspondra à l'élément ciblé
let elementById = document.getElementById('monId');

// Je selectionne un élément en fonction d'une classe
// retourne un tableau d'éléments qui auront la classe ciblée
let elementsByClassName = document.getElementsByClassName("maClasse");

// Je selectionne un element en fonction de son tag (h1, div, span ...)
// retourne un tableau d'éléments qui auront la classe ciblée
let elementsByTagName = document.getElementsByTagName("body");

````
  
- Supprimer un élément du DOM 

Quand on supprime un élément du DOM, il n'est plus visible sur la page web

````javascript
// Ici, je réccupére tous les H1 de ma page
let element = document.getElementsByTagName('h1');

// Ici je réccupére le premier H1 de ma page
element = element[0];

// Ici je supprime le premier H1. Il ne sera plus affiché
element.remove()
````

- Ecrire dans un élément du DOM

````javascript
let element = document.getElementById("monId");
// Ecrit un paragraphe contenant Hello world dans pa mage. Le HTML sera interprété par le navigateur
element.innerHTML = '<p>Hello world</p>';
// Ecrit Hello world dans ma page web. Le html ne sera pas interprété par le navigateur
element.innerText = 'Hello world'

````  

- Modifier du css dans un élément du DOM
````javascript
let element = document.getElementById("monId");
// Ici j'ajoute un background color bleu
element.style.backgroundColor = 'blue';
// Ici je cache mon élément (propriété display à none)
element.style.display = 'none';
````

- Ajouter / Supprimer une classe dans un élément du DOM
````javascript
let element = document.getElementById("monId");
// Ajoute la classe maNouvelleClasse à mon élément 
element.classList.add('maNouvelleClasse')
// Supprime la classe maNouvelleClasse à mon élément 
element.classList.remove('maNouvelleClasse')
````

###### Evenements temporels

Permet d'executer du code source en fonction d'un timer

- setInterval
  
Permet d'executer du code source toutes les X milliseconde (paramètre 1). La fonction passées en paramètre 2 est appelé dès que le timer est écoulé. Ensuite, le timer se relance

```javascript
// Fonction qui fait un alert
function alert2sec(){
    alert('2 secondes écoulées');
}

// Cette fonction sera appelée toutes les 2 secondes
let intervalId = setInterval(2000, alert2sec);

// Ceci supprime notre setInterval et arrête le timer
clearInterval(intervalId)
```

- setTimeout

Identique au setInterval sauf qu'une fois le timer à 0, il nest pa relancé

```javascript
function alert2sec(){
    alert('2 secondes écoulées');
}

// Appel de la fonction alert2sec toutes les 2 secondes
let timeOutId = setTimeout(2000,alert2sec);
// Permet d'annuler notre timeOUt
clearTimeout(timeOutId);
```

###### Evenements

Permet d'interagir avec les événements utilisateurs. 
Il en existe pleins. On peut les combiner

Exemple : Fonction qui se déclanche au clic d'un utilisateur 

````javascript
let element = document.getElementById('monId');
element.onclick = function(){
    alert('CLICKED !');
};
````

###### Event listeners
Permet de créer un événement. La fonction addEventListener prend 2 paramètres
le type d'événement et la fonction à appelé lors du déclanchement de l'evenement

````javascript
function clickOnMe(){
    alert('Tu as cliqué sur moi !');
}

let element = document.getElementById('monId');
element.addEventListener('click', clickOnMe )
````

###### Local Storage

Permet de stocker des données dans le navigeur. 
Ces données seront conservées même si l'utilisateur se déconnecte.
Elles sont accessible à l'utilisateur (attention aux données sensibles).
Elles seront supprimées si l'utilisateur les supprime. Il permet de stocker des éléments clé => valeur

````javascript
let ls = localStorage;
// Permet de stocker cle=>valeur dans le ls
ls.setItem('cle', 'valeur');
// Permet de réccupérer 'valeur' dans la varible cle
let cle = ls.getItem('cle');

// Supprime la clé 'cle' du local storage
ls.removeItem('cle');

// Permet de supprimer toutes les clé => valeur du local storage
ls.clear()
````



###### Cookie 

Normalement, les cookies sont initiés par les serveurs. 

Aussi des fichiers textes cle => valeur qui sont stockés sur le navigateur

Nous devons absolument demander le consentement à l'utilisateur

Un cookie a un temps de vie maximum après lequel il est détruit

Un cookie peut être limité à une page, a un domaine mais peut aussi être partagé.




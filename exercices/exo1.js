function displayImg(animal){
    let monImage = document.getElementById("monImage");
    monImage.style.display = 'block';

    monImage.src = 'images/'+animal + '.jpg';
    monImage.alt = 'Une image de ' + animal;
}
ls = localStorage;
let theme = ls.getItem("theme");

switchTheme(theme);

function switchTheme(mode){
    ls = localStorage;
    let body = document.getElementsByTagName('body')[0];
    let darkButton = document.getElementById('darkButton');
    let lightButton = document.getElementById('lightButton');

    if(mode == 'dark'){
        body.classList.add('dark');
        darkButton.classList.add('display-none');
        lightButton.classList.remove('display-none');


        ls.setItem('theme', 'dark');
    }

    if(mode == 'light'){
        body.classList.remove('dark');
        darkButton.classList.remove('display-none');
        lightButton.classList.add('display-none');

        ls.setItem('theme', 'light');
    }
}
function success(pos){

    let latitude = pos.coords.latitude;
    let longitude = pos.coords.longitude;

    let iframe = document.getElementsByTagName("iframe")[0];

    iframe.src = 'https://www.google.com/maps?q=@'+latitude+','+longitude+'&output=embed';
}

function echec(){
    alert('Imposssible d\'acceder à votre localisation');
}

function geolocalizeMe(){
    let coords = navigator.geolocation.getCurrentPosition(success, echec);
}
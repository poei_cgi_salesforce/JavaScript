let traductionArray = {
    'fr': {
        '{title_page}': 'Titre de la page',
        '{subtitle_page}': 'Sous titre de la page',
        '{paragraph}': 'Un paragraphe en français',
        '{google_link}': 'Aller sur google !',
        '{twitter_link}': 'Aller sur twitter'
    },
    'en-US': {
        '{title_page}': 'Title of the page',
        '{subtitle_page}': 'Subtitle of the page',
        '{paragraph}': 'An english paragraph',
        '{google_link}': 'Go to google !',
        '{twitter_link}': 'Go to twitter'
    }
}


Object.keys(traductionArray[navigator.language]).forEach(elem => {
    let traduction = traductionArray[navigator.language][elem];
    document.body.innerHTML = document.body.innerHTML.replace(elem, traduction);
})


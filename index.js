/*
 *    Je suis un commentaire sur plusieurs lignes
 */

// Commentaire sur une seule ligne

// Numéro de CB 1111 1111 1111 1111 1111

//  alert('Ma premiere instruction JavaScript');

// console.log('Bienvenue dans mon premier programme JS !');


// console.error('Si tu es un hacker DEGAGE');

function maFonctionJavaScript(){
    debugger;
    nom = document.getElementById('nom').value;
    prenom = document.getElementById('prenom').value;

    console.log(nom + ' ' + prenom);
}


let students = [];

student1 = [];
student1["nom"] = 'Delorme';
student1["prenom"] = 'Aurélien';

student2 = [];
student2["nom"] = 'MBappe';
student2["prenom"] = 'Kyllian';

students.push(student1);
students.push(student2);
console.log(students);
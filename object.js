let pc = {
    marque: "Dell",
    nombreCoeur: 8,
    ram: 32,
    model: "XPS",

    additionner : function(nb1, nb2){
        return nb1 + nb2;
    }
}

console.log(typeof pc);
console.log(pc);
console.log("Mon pc possède "+ pc.ram +' Go de ram');

pc.color = 'red';

console.log(pc);

console.log(pc.toString());


let resultatAddition = pc.additionner(1,2);
console.log(resultatAddition);
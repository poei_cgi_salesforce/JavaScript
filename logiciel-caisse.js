// Un tableau clé => valeur dont chacun représente un jeu de notre stock
let game1 = [];
game1["nom"] =  "Hogwarts Legacy";
game1["prix"] = 80.50;
game1["image"] = "https://cdn1.epicgames.com/offer/e97659b501af4e3981d5430dad170911/EGS_HogwartsLegacy_AvalancheSoftware_S1_2560x1440-2baf3188eb3c1aa248bcc1af6a927b7e";


// 2ème tableau clé valeur
let game2 = [];
game2["nom"] =  "GTA 5";
game2["prix"] = 70.87;
game2["image"] = 'https://static1.millenium.org/articles/3/11/30/63/@/18191-gta456-article_m-1.jpeg';

// troisième tableau clé valeur
let game3 = [];
game3["nom"] =  "Fifa 2023";
game3["prix"] = 50.3;
game3["image"] = 'https://cdn1.epicgames.com/offer/f5deacee017b4b109476933f7dd2edbd/EGS_EASPORTSFIFA23StandardEdition_EACanada_S1_2560x1440-aaf9c5273c27a485f2cce8cb7e804f5c';

// quatrième tableau clé valeur
let game4 = [];
game4["nom"] =  "War zone";
game4["prix"] = 0;
game4["image"] = 'https://blog.fr.playstation.com/tachyon/sites/10/2022/11/914146e39d255847514febc4f3491e9e85091f5f-scaled.jpg?resize=1088%2C612&crop_strategy=smart';


// tableau numéroté (cours)
let gamesInShop = [];

// Push : ajout d'un élément dans un tableau (cours)
// Pas dans le cours : tableau numéroté qui contient
// un tableau clé valeur
gamesInShop.push(game1);
gamesInShop.push(game2);
gamesInShop.push(game3);
gamesInShop.push(game4);

let panier = [];

// Utilisation de fonction (onclick dans html cours)
function displayGameInShop(){

    // Pas dans le cours on réccupére un élément de notre page
    // celui qui a l'id resultsStock
    let resultSection = document.getElementById("resultsStock");
    // cours : chaine de caractères vide
    let str = '';

    // Dans le cours : on parcours notre tableau numéroté
    gamesInShop.forEach( (elem, index) => {
        // On écrit dans notre chaine de caractère (cours concaténation)
        str +=
            '<div class="card col-md-4" style="width: 18rem;">\n' +
            '<img style="max-width: 150px" class="card-img-top" src='+ elem["image"] +' alt="Card image cap">' +
            '  <div class="card-body">\n' +
            '    <h5 class="card-title">'+elem['nom']+'</h5>\n' +
            '    <p class="card-text">'+elem['prix']+'</p>\n' +
            '    <a onclick="addPanier(\''+index+'\')" class="btn btn-primary">Ajouter au panier</a>\n' +
            '  </div>\n' +
            '</div>'
    });

    // Pas dans le cours : On insere du HTML dans notre page
    resultSection.innerHTML = str;
}

// Fonction qui permet l'ajout d'un élément au panier
// Cette fonction prend en paramètre l'index du produit que je souhaite ajouter
// Dans mon tableau de stock (le grand tableau du début)
function addPanier(index){
    // De base mon élément n'est pas déjà dans le panier
    let exist = false;

    // Je parcours tous les éléments de mon panier
    panier.forEach(elem => {
        // Si pendant mon parcours, je retrouve l'élément que je souhaite ajouter
        // Ca veut dire que l'élément est déjà dans mon panier
        if(elem == gamesInShop[index]){
            // Vu que l'élément est déjà dans mon panier,
            // Je n'en ajoute pas un nouveau mais j'augmente la quantité
            elem["quantite"] = elem["quantite"] + 1;
            // Je met un flag pour indiquer que le produit est déjà dans le panier
            exist = true;
        }
    });

    // Si le produit n'est pas dans le panier je l'ajoute
    if(exist == false){
        // J'ajoute le produit dans mon panier
        panier.push(gamesInShop[index]);
        // Le produit n'était pas dans le panier.
        // La quantité sera donc égale à 1
        panier[panier.length-1]["quantite"] = 1;
    }

    // J'appelle une fonction qui est en charge d'afficher le contenu du panier
    displayPanier(panier);
}

function removePanier(index) {
    // Je supprime l'élément de mon tableau
    panier.splice(index, 1);
    // Je demande à la fonction displayPanier d'afficher mon panier
    displayPanier(panier);
}

// Fonction en charge d'afficher le panier
// Elle prend en paramètre notre panier
function displayPanier(panier){
    // On déclare une chaine de caractères vide
    let str = '';

    // On réccupére la div qui sera utile à l'affichage
    divPanier = document.getElementById('panier');

    // On déclare le total du panier égal à 0
    let sum = 0;
    // On déclare le nombre d'élément du panier égal à 0
    let nbElem = 0;

    // On parcours tous les éléments de notre panier
    panier.forEach((element, index) => {
        // Pour chacun des éléments, on multiplie le prix unitaire par la quantité
        sum += element["prix"] * element["quantite"];

        // Pour chacun des éléments on met à jour la quantité de produits dans le panier
        nbElem += element["quantite"];

        // On va aller ajouter à notre chaine de caractères tous les éléments présent dans le panier
        // pour cela on utilise le principe de la concaténation
        str +=
            '<div class="card col-md-4" style="width: 18rem;">\n' +
            '<img style="max-width: 150px" class="card-img-top" src='+ element["image"] +' alt="Card image cap">' +
            '  <div class="card-body">\n' +
            '    <h5 class="card-title">'+element['nom']+' ('+element["quantite"]+')</h5>\n' +
            '    <p class="card-text">'+element['prix']+'</p>\n' +
            '    <a onclick="removePanier(\''+index+'\')" class="btn btn-danger">Supprimer du panier</a>\n' +
            '  </div>\n' +
            '</div>';
    });

    // On réccupére notre div qui affiche le nombre d'élément dans le panier
    // On inscrit dedans le nombre d'éléments calculé au dessus
    document.getElementById('nbElemPanier').innerText = nbElem + ' éléments';

    // On réccupére notre div qui affiche le total de nos achat en euros
    // On affiche le total
    document.getElementById('nbEuros').innerText = sum.toFixed(2) + ' euros';

    // On affiche tous nos produits dans le panier
    divPanier.innerHTML = str;
}
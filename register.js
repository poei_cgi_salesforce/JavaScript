function validateForm() {
    let existError = false;
    let username = document.getElementById("username").value;
    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;
    let confirm = document.getElementById("confirm").value;

    let letters = /^[A-Za-z]+$/;
    let emailValidator = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let lettersValidator = /[a-zA-Z]/g
    let numberValidator = /[0-9]/g

    if (username == '') {
        alert('Veuillez saisir un nom d\'utilisateur');
        existError = true;
    }

    if (!username.match(letters)) {
        alert('Le champ username doit contenir que des lettres');
        existError = true;
    }

    if(email == ''){
        alert('Le champ email doit être saisie !');
        existError = true;
    }

    if(!email.match(emailValidator)){
        alert('Le champs email n\'est pas valide !');
        existError = true;
    }

    if(password == ''){
        alert('Veuillez saisir un mot de passe');
        existError = true;
    }

    if(password.length < 8){
        alert('Le mot de passe est trop court');
        existError = true;
    }

    if(!password.match(numberValidator) || !password.match(lettersValidator)){
        alert('Attention : le mot de passe doit contenir de lettres et du texte');
        existError = true;
    }

    if(password != confirm){
        alert('Votre passe n\'est pas confirmé correctement !');
        existError = true;
    }

    if(!existError){
        alert('Félicitations !!!');
    }
}
